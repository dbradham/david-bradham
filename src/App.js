import './App.css';

import {
  HashRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Toolbar from './components/Toolbar';

import Home from './screens/Home';
import CV from './screens/CV';
import Mapping from './screens/Mapping';
import Development from './screens/Development'

function App() {
  return (
    <Router>
      <Toolbar />
      <Switch>
        <Route path="/development">
          <Development />
        </Route>
        <Route path="/mapping">
          <Mapping />
        </Route>
        <Route path="/cv">
          <CV />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
