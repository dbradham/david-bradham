import React from 'react';
import './Home.css';
import { RiArrowRightSLine } from 'react-icons/ri';
import { Link } from 'react-router-dom';

const Home = () => {
    const bg = require('../assets/images/background.jpeg').default;
    const mobileBg = require('../assets/images/thirdStreet.jpg').default;

    return <div className="wrapper home-wrapper">
        <img src={bg} alt="Rocky Mountains" className="background-image"></img>
        <img src={mobileBg} alt="David in Downtown Austin" className="mobile-background-image"></img>
        <div className="home-text">
            <Link to="/cv">
                <h1 className="home-header">David Bradham</h1>
            </Link>
            <ul>
                <li>
                    <RiArrowRightSLine />
                    <Link to="/development">Software Engineer</Link>
                </li>
                <li>
                    <RiArrowRightSLine />
                    <Link to="/mapping">Mapping expertise</Link>
                </li>
            </ul>
        </div>
    </div>
};

export default Home;