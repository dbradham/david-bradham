import React from 'react';
import Map from '../components/Map';
import StaticMapDisplay from '../components/StaticMapDisplay';
import './Mapping.css';

const fema_maps = [
    require('../assets/images/fema_assessment/Austin_County.png'), 
    require('../assets/images/fema_assessment/Brazoria_County.png'),
    require('../assets/images/fema_assessment/Chambers_County.png'), 
    require('../assets/images/fema_assessment/Colorado_County.png'),
    require('../assets/images/fema_assessment/Fort_Bend_County.png'), 
    require('../assets/images/fema_assessment/Galveston_County.png'),
    require('../assets/images/fema_assessment/Harris_County.png'), 
    require('../assets/images/fema_assessment/Matagorda_County.png'),
    require('../assets/images/fema_assessment/Montgomery_County.png'), 
    require('../assets/images/fema_assessment/Walker_County.png'),
    require('../assets/images/fema_assessment/Waller_County.png'), 
    require('../assets/images/fema_assessment/Wharton_County.png'),
];
const alt_maps = [
    require('../assets/images/alt_assessment/Austin_County.png'), 
    require('../assets/images/alt_assessment/Brazoria_County.png'),
    require('../assets/images/alt_assessment/Chambers_County.png'), 
    require('../assets/images/alt_assessment/Colorado_County.png'),
    require('../assets/images/alt_assessment/Fort_Bend_County.png'), 
    require('../assets/images/alt_assessment/Galveston_County.png'),
    require('../assets/images/alt_assessment/Harris_County.png'), 
    require('../assets/images/alt_assessment/Matagorda_County.png'),
    require('../assets/images/alt_assessment/Montgomery_County.png'), 
    require('../assets/images/alt_assessment/Walker_County.png'),
    require('../assets/images/alt_assessment/Waller_County.png'), 
    require('../assets/images/alt_assessment/Wharton_County.png'),
];

const fema_writeup = [
    <p>
        The goal of this project was to assess the flood risk of every wastewater treatment plant in the Texas Commission of Environmental Quality's Region 12. 
        Region 12 refers to the greater Houston area and includes Austin, Brazoria, Chambers, Colorado, Fort Bend, Galveston, Matagorda, Harris, Liberty, Walker, Waller, and Wharton Counties. 
        One of my primary tasks during my internship was to verify the locations of the 3,098 wastewater treatment plants which were regulated by TCEQ.
    </p>,
    <p>
        The data used for this project included shapefiles representing the extent of the state of Texas, the extent of all the counties in Texas, the location of every wastewater treatment plant in the state, (represented as points) water bodies, dams, reservoires, major road systems, urban areas, and, importantly, all of the available FEMA FIRM (Flood Insurance Risk Management) maps.
        The first step was to use ArcMap's clip function to reduce the extent of all of the shapefiles to Region 12.
        After this, multiple shapefiles were generated from the FIRM maps.
        These shapefiles represented 500 year flood zones, 100 year floodplains, floodways, minimal risk flood zones, and reduced risk flood zones.
        At this point, I was able to use each of these flood zone shapefiles as an input in ArcMap's intersect tool to display which wastewater treatment plants were in floodways or a specific type of flood zone.
    </p>,
    <p>
        The major problem with this approach was that FEMA FIRM Maps require the work of multiple geologists to generate.
        This means that they are expensive, and many of the counties of interest had never drawn a FIRM Map.
        For example, all of the WWTPs in Brazoria and Matagorda County would be considered "safe" from flooding using this paradigm because there is no available FIRM maps.
        Of course this is simply not true, many of these WWTPs have flooded in the past and will flood again.
        In light of this, a secondary risk assessment was created using the observed flood extent of Hurricane Harvey. (see below)
    </p>
];

const alt_writeup = [
    <p>
        The goal of this project was to assess the flood risk of every wastewater treatment plant in the Texas Commission of Environmental Quality's Region 12. Region 12 refers to the greater Houston area and includes Austin, Brazoria, Chambers, Colorado, Fort Bend, Galveston, Matagorda, Montgomery, Harris, Liberty, Walker, Waller, and Wharton Counties. One of my primary tasks during my internship was to verify the locations of the 3,098 wastewater treatment plants which were regulated by TCEQ. 
    </p>,
    <p>
        The data used for this project included shapefiles representing the extent of the state of Texas, the extent of all the counties in Texas, the location of every wastewater treatment plant in the state, (represented as points) water bodies, dams, reservoires, major road systems, urban areas, and, importantly, the observed flood extent from Hurrican Harvey. The first step was to use ArcMap's clip function to reduce the extent of all of the shapefiles to Region 12. I was able to use the observed flood extent shapefiles as one input in ArcMap's intersect tool and used the wastewater treatment plant shapefile as the other input. The result was a shapefile that displayed wheather each wastewater treatment plant was located in an area that was observed to have flooded.
    </p>
];

const Mapping = () => {
    return (
        <div style={styles.container}>
            <h2 className="mapping-header">Interactive Web Map of the Edwards Aquifer</h2>
            <div className="interactive-map-container map-container">
                <div className="writeup white-box">
                    <p>This map was created by using <a href="https://react-leaflet.js.org/">React Leaflet</a>.</p>
                    <p>The Edwards Aquifer supplies water to approximately 2 million people.</p>
                    <p>
                        The contributing zone, which can also be thought of as the drainage area for the aquifer lies in the Texas Hill Country. 
                        The underlying geology in this region is virtually impermeable, and it is at a much higher elevation than the other zones.
                    </p>
                    <p>
                        The recharge zone is characterized by sinkholes and vanishing streams. A history of tectonic movements has allowed for water
                        to be stored in underground cavities within the karst limestone. Some sinkholes can deposit water at a rate of 2,000 gallons per second.
                    </p>
                    <p>Once the water has gone underground in the recharge zone, it moves quickly to the artesian zone. 
                        In the artesian zone, the water is stuck in between impermeable limestone and del rio clay soils. When more water enters
                        the artesian zone, it puts pressure on the water that was already there to leave. This is how springs can move against the 
                        forces of gravity to burst through the soil.
                    </p>
                </div>
                <div style={styles.rightContent}>
                    <Map />
                </div>
            </div>

            <h2 className="mapping-header">Series of FEMA Flood Risk Assessment Maps</h2>
            <StaticMapDisplay writeup={fema_writeup} maps={fema_maps} />

            <h2 className="mapping-header">Series of Alternative Flood Risk Assessment Maps</h2>
            <StaticMapDisplay writeup={alt_writeup} maps={alt_maps} />
      </div>
    );
};

export default Mapping;

const styles = {
    container: {
      marginTop: 60,
      backgroundColor: '#dddddd',
      padding: 25,
    },
    header: {
        color: '#1f4729',
        marginLeft: 30
    },
    paragraph: {
        marginTop: 75,
        marginLeft: 15,
    },
    rightContent: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginRight: 20
    },
    stats: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: 120,
        width: '100%'
    }
  }