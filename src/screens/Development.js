import React from 'react';
import DevelopmentLanguage from '../components/DevelopmentLanguage';
import './Development.css';

const Development = () => {
    const js = require('../assets/images/js.png').default;
    const py = require('../assets/images/python.jpeg').default;
    const cSharp = require('../assets/images/c#.jpeg').default;

    const data = [
        {
            name: 'JavaScript',
            img: js,
            writeup: 'The website you are using right now is written in JavaScript. To be more specific, it is a serverless ReactJS application. The source code can be viewed here: https://gitlab.com/dbradham/david-bradham. When I want to build a simple application, or just want to get a web application up and running quickly, I like using JavaScript. I have also learned mobile development from the React-Native library.',
            skills: [
                'ReactJS',
                'React-Native',
                'NodeJS',
                'jQuery',
                'Web scraping with Express and Axios'
            ]
        }, 
        {
            name: 'Python',
            img: py,
            writeup: 'Python was the first programming language I learned. I frequently write python scripts to parse, clean, or perform a data transformation. I have more experience performing data analysis in python than web development.',
            skills: [
                'Flask',
                'Scikit-learn',
                'Pandas',
                'NumPy',
                'MatplotLib',
                'Web scraping with Requests, Beautiful Soup, and Urllib'
            ]
        }, 
        {
            name: 'C#',
            img: cSharp,
            writeup: 'I learned this language from working in the industry and it quickly became one of my favorites. If the server is going to be performing process-intensive tasks at a very high frequency, this is my language of choice.',
            skills: [
                '.NET MVC Razor Framework',
                'Entity Framework',
                'LingQ',
                'Quartz Scheduled Jobs',
                'Web scraping with Html Agility Pack'
            ]
        }
    ];

    const developmentLanguages = data.map(datum => {
        return <DevelopmentLanguage data={datum} />;
    });

    return (
        <div className="wrapper dev-container">
            <h1>What tools do I use to develop?</h1>
            <p className="dev-tools-writeup white-box">I am always striving to learn and expand my understanding when it comes to "development tools", better known as programming languages. My ability with one tool versus another often depends on how much I have practiced with those tools in recent months. The three programming languages listed below are currently the three I am the most comfortable developing with.</p>
            <div className="dev-languages">
                {developmentLanguages}
            </div>
        </div>
    );
};

export default Development;