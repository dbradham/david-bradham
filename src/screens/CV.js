import React, { useState } from 'react';
import './CV.css';
import Backdrop from '../components/Backdrop';

const CV = () => {
    const [showMap, setShowMap] = useState(false);

    const profile = require('../assets/images/profile-miami.jpeg').default;

    const name = 'David Bradham';
    const job = 'Software Engineer';

    const phone = '832-594-9020';
    const email = 'davebradham@gmail.com';
    const location = 'Austin, TX';

    const banyan = require('../assets/images/banyan.png').default;
    const tceq = require('../assets/images/tceq.png').default;
    const ut = require('../assets/images/ut.png').default;
    const uta = require('../assets/images/uta.png').default;
    const dailyDinners = require('../assets/images/Logo with Plate-04.png').default;

    const sqlServer = require('../assets/images/sqlServer.jpeg').default;
    const postgreSql = require('../assets/images/postgreSQL.png').default;
    const mySql = require('../assets/images/mySql.png').default;
    const firestore = require('../assets/images/cloudFirestore.png').default;

    const map = require('../assets/images/state_rep.jpg').default;

    const mapComponent = showMap ? <div className="map-img-wrapper">
        <button
            className="close-map-btn"
            onClick={() => setShowMap(false)}
        >Close Map</button>
        <img
            src={map}
            alt="Map for State Representative of Texas Hour District 73"
            className="state-rep-map"
        ></img>
        </div> 
    :
    null;

    const showTheMap = () => {
        setShowMap(true);

        const body = document.querySelector('#root');

        body.scrollIntoView({
            behavior: 'smooth'
        }, 500);
    };

    return <div className="wrapper cv">
        <Backdrop isOpen={showMap} onClick={() => setShowMap(false)}/>
        {mapComponent}
        <div className="top-of-cv">
            <div className="top-of-cv-profile">
                <img 
                    src={profile}
                    alt="Image of David"
                    className="cv-profile-pic"
                ></img>
                <div className="top-of-cv-profile-detail">
                    <span className="cv-name">{name}</span>
                    <span className="cv-job">{job}</span>
                </div>
                <div className='top-of-cv-profile-bio'>
                    <p>
                        My main professional pursuit is to work software solutions which make the world a better place, and are disruptive by nature.
                    </p>
                    <p>
                        I love working on issues related to the environment.
                        Growing up in Texas, I have been priviledged to experience the benefits of technology first hand.
                        It's exciting for me to apply many of these technologies to solve problems that matter.
                    </p>
                </div>
            </div>
            <div className="top-of-cv-contact">
                <span>Phone: {phone}</span>
                <span>Email:  {email}</span>
                <span>Location: {location}</span>
            </div>
        </div>
        <div className="bottom-of-cv">
            <div className="bottom-of-cv-main">
                <div className="cv-bottom-entry white-box bio-box">
                    <p>
                        My main professional pursuit is to work software solutions which make the world a better place, and are disruptive by nature.
                    </p>
                    <p>
                        I love working on issues related to the environment.
                        Growing up in Texas, I have been priviledged to experience the benefits of technology first hand.
                        It's exciting for me to apply many of these technologies to solve problems that matter.
                    </p>
                </div>
                <h1>Work Experience</h1>
                
                <div className="cv-bottom-entry white-box">
                    <div className="cv-main-header">
                        <img 
                            src={banyan}
                            alt="Banyan Water Logo"
                            className="cv-main-logo banyan-logo"
                        ></img>
                        <h2>Banyan Water; Feb 2019 - Present</h2>
                    </div>
                    <h3>Software Team Lead / Innovation Engineer (January 2022 - Present)</h3>

                        <ul>
                            <li>Facilitate day-to-day activities of an agile team consisting of multiple engineers and product owner</li>
                            <li>Collaborate with other teams in the business to define technology requirements and solutions</li>
                            <li>Contribute to technical solutions in C# .NET, NodeJS, ReactJS, Flutter, and python</li>
                        </ul>
                        <h3>Software Engineer (January 2020 - December 2021)</h3>
                        <ul>
                            <li>Researched and implemented new methodologies to meet business needs</li>
                            <li>Participated in sprint planning meetings as part of the team's scrum</li>
                            <li>Performed QA and code review of teammates' work</li>
                        </ul>
                        <h3>Junior Software Engineer (June 2019 - December 2019)</h3>
                        <ul>
                            <li>Became one of three active maintainers of a C# platform</li>
                            <li>Full stack development involved Razor pages and jQuery</li>
                            <li>Worked on an agile team with bi-weekly sprints and releases</li>
                        </ul>
                        <h3>Water Conservation Intern (Feb. 2019 - May 2019)</h3>
                        <ul>
                            <li>Worked to streamline logistic workflow and data input for new customers</li>
                            <li>Installed new firmware versions on incoming IoT devices</li>
                        </ul>
                </div>

                <div className="cv-bottom-entry white-box">
                    <div className="cv-main-header">
                        <img 
                            src={tceq}
                            alt="TCEQ Logo"
                            className="cv-main-logo tceq-logo"
                        ></img>
                        <h2>Texas Commission on Environmental Quality (TCEQ); Summer of 2018</h2>
                    </div>
                    <ul>
                        <li>Mickey Leland Environmental Internship Program</li>
                        <ul>
                            <li>Worked in the department of water on the Water Quality Assessment Team</li>
                            <li>Geolocated all constructed and registered (~3,000) waste water treatments plants in the state of Texas as of August 2018</li>
                            <li>Geolocated approximately 10,000 lift stations</li>
                            <li>Geolocated approximately 450 concentrated animal feed operations (developed some vegan eating habbits after this)</li>
                            <li>Created a map for use by the office a state congressperson, <span onClick={() => showTheMap()} className="cv-click-here">click here to view</span></li>
                        </ul>
                    </ul>
                </div>

                <h1>An Application I have Built & Maintain</h1>
                <div className="cv-bottom-entry white-box">
                    <div className="cv-main-header">
                        <img 
                            src={dailyDinners}
                            alt="Daily Dinners Logo"
                            className="cv-main-logo dd-logo"
                        ></img>
                        <h2>Daily Dinners</h2>
                    </div>
                    <ul>
                        <li><a className="dd-link" href="https://daily-dinners.web.app">Click here to view the application</a></li>
                        <li>Daily Dinners is a Bradham family business</li>
                        <li>Launched in 2020, our mission is to provide Comfort Food Recipes for people with significant food allergens</li>
                    </ul>
                </div>

                <h1>Education</h1>

                <div className="cv-bottom-entry white-box">
                    <div className="cv-main-header">
                        <img 
                            src={ut}
                            alt="University of Texas Longhorns Logo"
                            className="cv-main-logo ut-logo"
                        ></img>
                        <h2>University of Texas at Austin</h2>
                    </div>
                    <ul>
                        <li>Class of 2019.</li>
                        <li>Bachelor of Arts in Geographic Information Systems (GIS)</li>
                        <li>Elements of Computing Certificate (18 hours of CS courses with 12 hours upper level)</li>
                        <li>Major GPA: 3.79</li>
                        <li>Total GPA: 3.5</li>
                    </ul>
                </div>

                <div className="cv-bottom-entry white-box">
                    <div className="cv-main-header">
                        <img 
                            src={uta}
                            alt="University of Texas at Arlington Logo"
                            className="cv-main-logo uta-logo"
                        ></img>
                        <h2>University of Texas at Arlington</h2>
                    </div>
                    <ul>
                        <li>Attended from 2015-2016 while in the CAP program</li>
                        <li>Total GPA: 3.5</li>
                    </ul>
                </div>
            </div>

            <div className="bottom-of-cv-cutaway">
                <h1>Skills</h1>
                <ul className='white-box'>
                    <li>Programming Languages</li>
                    <ul>
                        <li>C#</li>
                        <ul>
                            <li>.NET</li>
                            <li>Azure Functions</li>
                            <li>MVC</li>
                            <li>Entity Framework & linQ</li>
                        </ul>
                        <li>Javascript</li>
                        <ul>
                            <li>ReactJS</li>
                            <li>"Vanilla" (document object) JS in browser</li>
                            <li>NodeJS</li>
                        </ul>
                        <li>Python</li>
                        <ul>
                            <li>Flask</li>
                            <li>Data Analysis: numpy, pandas, matplotlib, and sci-kit learn</li>
                        </ul>
                        <li>Dart & Flutter Framework</li>
                        <li>PHP in LAMP stack</li>
                        <li>Java Spring</li>
                    </ul>
                    <li>Database Management</li>
                    <ul>
                        <li>SQL Server</li>
                        <li>PostgreSQL</li>
                        <li>MySQL</li>
                        <li>Cloud Firestore (non-SQL)</li>
                        <li>Strapi (non-SQL)</li>
                    </ul>
                    <li>Unix & Powershell</li>
                    <li>Source Control</li>
                    <ul>
                        <li>Github</li>
                        <li>Bitbucket</li>
                        <li>Gitlab</li>
                    </ul>
                    <li>HTML 5</li>
                    <li>CSS 3</li>
                    <li>GIS</li>
                    <ul>
                        <li>QGIS</li>
                        <li>ArcGIS</li>
                        <li>ERDAS Imagine</li>
                        <li>Google Earth Engine</li>
                        <li>Google Earth Pro</li>
                    </ul>
                </ul>
            </div>
        </div>
    </div>
};

export default CV;