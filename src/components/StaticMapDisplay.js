import React, { useState } from 'react';
import { BiArrowToRight, BiArrowToLeft } from 'react-icons/bi';

const StaticMapDisplay = ({ maps, writeup }) => {
    const [mapIndex, setMapIndex] = useState(0);

    const nextMap = () => {
        let newIndex = mapIndex + 1;
        if (newIndex >= maps.length) {
            newIndex = 0;
        }

        setMapIndex(newIndex);
    };

    const previousMap = () => {
        let newIndex = mapIndex - 1;
        if (newIndex < 0) {
            newIndex = maps.length - 1;
        }

        setMapIndex(newIndex);
    };

    return (
        <div className="static-map-wrapper">
            <div className="static-map-container map-container">
                <div>
                    <div className="static-map-btns">
                        <button className="static-map-btn" onClick={() => previousMap()}>
                            <BiArrowToLeft size={50} />
                            Previous
                        </button>
                        <button className="static-map-btn" onClick={() => nextMap()}>
                            Next
                            <BiArrowToRight size={50} />
                        </button>
                    </div>
                    <div className="writeup white-box">
                        {writeup}
                    </div>
                </div>
                <img
                    src={maps[mapIndex].default}
                    alt="Map created by David during Mickey Leland Internship in 2018"
                    className="static-map"
                ></img>
            </div>
        </div>
    );
};

export default StaticMapDisplay;