import React from 'react';
import  { Polygon, Tooltip } from 'react-leaflet';

const ChloroplethPolygon = ({ positions, text, color }) => {
    return (
        <Polygon color={color} positions={positions}>
            <Tooltip>{text}</Tooltip>
        </Polygon>
    );
};

export default ChloroplethPolygon;

const styles = {

};