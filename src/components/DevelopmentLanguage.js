import React from 'react';

const DevelopmentLanguage = ({ data }) => {
    const imgSrc = data.img;
    const name = data.name;

    const skills = data.skills.map(skill => {
        return <li>{skill}</li>;
    });

    const alt = name + " Logo";
    return (
        <div className="dev-lang-container white-box">
            <h3 className="dev-lang-name">{name}</h3>
            <img
                src={imgSrc}
                className="dev-img"
                alt={alt}
            ></img>
            <p className="dev-lang-writeup">{data.writeup}</p>
            <ul>
                {skills}
            </ul>
        </div>
    );
};

export default DevelopmentLanguage;