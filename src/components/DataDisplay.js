import React from 'react';

const DataDisplay = ({ label, variance, value }) => {
    console.log('value', value);
    console.log('variance', variance);

    const labelFontSize = label.includes('Springs') ? 11 : 14;
    const valueFontSize = String(value).includes('feet') ? 11 : 14;

    const fontColor = variance > 10 || variance < -10 ? 'white' : 'black';

    return (
        <div style={containerStyle(variance)}>
            <span style={{ fontSize: labelFontSize, color: fontColor }}>{label}</span>
            <span style={{ fontSize: valueFontSize, color: fontColor }}>{value}</span>
        </div>
    );
};

export default DataDisplay;

const containerStyle = (variance) => {
    let opacity;
    let color;
    if (variance > 10) {
        opacity = 1;
        color = '28, 93, 163';
    } else if (variance < -10) {
        opacity = 0.7;
        color = '200, 0, 0';
    } else if (variance < 0) {
        opacity = 0.3;
        color = '200, 0, 0';
    } else {
        opacity = 1;
        color = '175, 231, 250';
    }
    const bg = `rgba(${color}, ${opacity})`;
    return {
        backgroundColor: bg,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        height: 110,
        width: 110,
        borderRadius: '50%',
        borderWidth: 2,
        borderColor: 'black'
    }
};