import React from 'react';
import './Toolbar.css';
import { Link } from 'react-router-dom';

const Toolbar = () => {
    return (
        <div className="toolbar-container">
            <Link className="toolbar-link" to="/">David Bradham</Link>
            <Link className="toolbar-link cv-long" to="/cv">Curriculum Vitae</Link>
            <Link className="toolbar-link cv-short" to="/cv">CV</Link>
            <Link className="toolbar-link" to="/development">Development</Link>
            <Link className="toolbar-link" to="/mapping">Mapping</Link>
        </div>
    );
};

export default Toolbar;