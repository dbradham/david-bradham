import React, { useState } from 'react';
import { MapContainer as Map, TileLayer } from 'react-leaflet'
import edwards from '../assets/data/edwards';
import importantLakes from '../assets/data/importantLakes';
import edwardsRivers from '../assets/data/edwardsRivers';
import ChloroplethPolygon from './CholorplethPolygon';
import 'leaflet/dist/leaflet.css';

const EdwardsMap = () => {
    const [lat, setLat] = useState(29.8);
    const [long, setLong] = useState(-99.25);
    const [zoom, setZoom] = useState(8);

    const rivers = edwardsRivers.features.map((feature) => {
        const coordinates = feature.geometry.coordinates[0].map((coord) => {
            return [coord[1], coord[0]];
        });
        const name = feature.properties.NAME;
        return <ChloroplethPolygon  color="#4395d1"
            text={name}
            positions={coordinates} />;
    });

    const lakes = importantLakes.features.map((feature) => {
        const coordinates = feature.geometry.coordinates[0].map((coord) => {
            return [coord[1], coord[0]];
        });
        const name = feature.properties.RES_NAME;

        return <ChloroplethPolygon  color="#143269"
            text={name}
            positions={coordinates} />;
    });

    const zones = edwards.features.map((feature) => {
        if (feature.properties.Name.includes('Contributing') ||
        feature.properties.Name.includes('Drainage')) {
            const coordinates = feature.geometry.coordinates[0][0].map((coord) => {
                return [coord[1], coord[0]];
            });
            return (
                <ChloroplethPolygon color="#9effc5" 
                text="Contributing area" 
                positions={coordinates}/>
            );
        } else if (feature.properties.Name.includes('Recharge')) {
            const coordinates = feature.geometry.coordinates[0][0].map((coord) => {
                return [coord[1], coord[0]];
            });
            return(
                <ChloroplethPolygon color="#ca88eb" 
                text="Recharge area" 
                positions={coordinates}/>
            );
        } else {
            const coordinates = feature.geometry.coordinates[0][0].map((coord) => {
                return [coord[1], coord[0]];
            });
            return (
                <ChloroplethPolygon color="#2c6da3" 
                text="Artesian area" 
                positions={coordinates}/>
            );
        }
    });

    return (
            <Map 
                center={[lat, long]} 
                zoom={zoom} 
                className="edwards-map"
            >
                <TileLayer
                    attribution='&copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {zones}
                {rivers}
                {lakes}
             </Map>
    );
};

export default EdwardsMap;